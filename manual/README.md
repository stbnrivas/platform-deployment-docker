# Table of Contents
1.  [Platform architecture description](#def-description)
2. [Installation and configuration](#def-configuration)
3. [Security Configuration](#def-sec-configuration)
4. [Usage Scenarios examples and annotations](#def-examples)
5. [Summary table of public instance endpoints](#def-endpoints)
***
# Introduction
This project represents the reference platform implementation of the EU H2020 [SynchroniCity](https://synchronicity-iot.eu/) project.

In order to make the deployment as easy and quick as possible, the platform architecure was defined with a container deployment approach using [Docker compose](https://docs.docker.com/compose/).

***
<a name="def-description"></a>
# 1. Platform architecture description

This section describes the components and the related containers used to make the working prototype of the architecture. Following diagram depicts the architecture and interactions among the containers.

 ![image alt text](images/docker-synch-arch.png)

This repository provides a `docker-compose.yml`, where all the architecture components are defined as containers (namely services).
If you want to use only a subset of them, comment with "#" the corresponding rows of the related container.

## Docker Networking  
The Docker Compose will create a bridge network (see [here](https://docs.docker.com/compose/networking/)), named **`main_synchronicity`**. The containers will be attached to this network and each one will have its own assigned IP and hostname, internal to the network. In particular, the container hostname will be equal to the name given in the “services” section of docker-compose file. Thus, each container can look up the hostname of the others; this will be important when configuring components to communicate to each other.

This can be checked by running:

* **`docker network ls`**, to see all the docker network, including the created one (**main_synhcronicity**), where all the containers will be attached to.

* **`docker inspect network main_synchronicity`**, to check IPs assigned to the running containers.

**NOTE**
As the network is a bridge, each port exposed by containers (e.g. 1026), will be mapped and also reachable in the machine where Docker was installed. Thus, if the machine is publicly and directly exposed, also that ports will be reachable, unless they were closed. 
***
## Components description
This section describes in detail each component included in the architecture.
As depicted in the figure, each component will have its persistence layer running as a separate Mongo DB Container. In particular, each one will have a mounted volume, that is a host folder, where the DB data will be stored.

### Orion Context Broker

The Context Broker is in charge of storing entities, which represent the mapped southbound devices and relative attributes (active, lazy, commands). It exposes NGSI APIs , in order to manage the lifecycle of an entity and related subscriptions.

It does not hold an historical view of entity values, this is performed by the Cygnus Connector and STH-Comet components.

**Public endpoint**: [https://services.synchronicity-iot.eu/api/context](https://services.synchronicity-iot.eu/api/context)

**API Reference**: [https://github.com/telefonicaid/fiware-orion#api-walkthrough ](https://github.com/telefonicaid/fiware-orion)

### Cygnus Connector

The Cygnus component has the role of connector between the Context Broker and several storage systems. In particular, it enables to create and store an historical view of the entities and related attributes issued by the Context Broker. 

It supports several «sinks», plugins to write in different persistence systems:

* MongoDB 
* MySQL
* CKAN
* PostgresSQL
* STH

Cygnus uses the subscription/notification feature of the Context Broker. A subscription is made in Orion on behalf of Cygnus, detailing which entities we want to be notified to Cygnus when an update occurs on any of those entities attributes.

In particular Cygnus listens for notifications on the **(internal) endpoint**: "[http://cygnus:5050/notify](http://cygnus:5050/notify)"

**Reference**:[ https://github.com/telefonicaid/fiware-cygnus](https://github.com/telefonicaid/fiware-cygnus)

### STH-Comet for SynchroniCity

The Short Time Historic (STH, aka. Comet) is the component in charge of managing (storing and retrieving) historical raw and aggregated time series information about the evolution in time of context data (i.e., entity attribute values) registered in the Context Broker instance. This is an extended version of FIWARE Short Time Historic (STH) - Comet, a component able to manage (storing and retrieving) historical context information as raw and aggregated time series context information. This version adds the   SynchroniCity Historical data retrieval API, to those already existing and provided by STH-Comet.

In this architecture, the **_formal_** way is used (see [here](https://fiware-sth-comet.readthedocs.io/en/latest/data-storage/index.html)). It uses the Cygnus component to register the raw and aggregated time series context information into a mongo container, then used by the STH component. The subscription of the entities then is still performed between Cygnus and Orion Context Broker.

Cygnus is configured by default (with the provided `agent.conf` file, see Configuration section), with the following sinks: 

* **Mongo-sink**: used for storing historical raw context information.
* **Sth-sink**: used for storing historical aggregated time series context information.

Both sinks store information in the MongoDB container (**mongo-cygnus**).

STH exposes an HTTP REST API to let external clients query the available historical context information:

*  **Raw (see [here ](https://fiware-sth-comet.readthedocs.io/en/latest/raw-data-retrieval/index.html))**
*  **Aggregated time series (see [here](https://fiware-sth-comet.readthedocs.io/en/latest/aggregated-data-retrieval/index.html))**
*  **SynchroniCity Historical data retrieval API**: The STH component, as Synchronicity customization, allows to get historical data related to an attribute of an entity, according to the API definition provided [here](https://synchronicityiot.docs.apiary.io/#reference/data-storage-api-historical/historical-data-retrieval).

**Public endpoint**: [ https://services.synchronicity-iot.eu/api/historical](https://services.synchronicity-iot.eu/api/historical)

**Original Fiware STH-Comet Reference**: [https://fiware-sth-comet.readthedocs.io/en/latest/index.html](https://fiware-sth-comet.readthedocs.io/en/latest/index.html)
**SynchroniCity custom version**:
[https://gitlab.com/synchronicity-iot/sth-comet-synchronicity/blob/master/README.md](https://gitlab.com/synchronicity-iot/sth-comet-synchronicity/blob/master/README.md)
### IoT Agent Manager (IDAS)

The IoT Agent Manager works as a proxy for scenarios where multiple IoT Agents offer different southbound protocols. It is a single administration endpoint for agent provisioning tasks, redirecting NGSI requests to the appropriate IoTAgent based on the declared protocol.

Each registered Agent is uniquely identified by the following two parameters :

* **Protocol**: Name of the protocol served by the IoTAgent
* **Resource-APIKey**: Unique pair of strings used to identify different IoT Agents for the same protocol.

It exposes the Subscription APIs to manage agent provisioning and the services to be redirected to the appropriate Agent.

**Public endpoint**:[ https://iot-agent.synchcity.eu]( https://iot-agent.synchcity.eu)

**Reference**:[ https://github.com/telefonicaid/iotagent-manager](https://github.com/telefonicaid/iotagent-manager)

### IoT Agent UltraLight

An IoT Agent is a component that lets a groups of devices send their data to and be managed from a FIWARE NGSI Context Broker using their own native protocols, such as UltraLight protocol for the measure payload. It manages the device registration, by specifying its attributes and related mapping to a NGSI context entity.
* Each **Device** will be mapped as an **Entity** associated to a Context Provider.
* Each **Measure** obtained from the device will be mapped to a different **entity attribute**.

Device measures can have three different behaviors:

* **Active** –  pushed from the device to the IoT agent.
* **Lazy** – pass device will wait for the IoT Agent to request for data. 
* **Commands** – Special attribute if the device can accept commands. 

The provision process is meant to provide the IoTA with the following information:

* Entity translation information: information about how to convert the data coming from the South Bound device into NGSI information. E.g. entity name, type and attributes that will be created in the entity.
* Southbound protocol identification: To identify a particular device when a new measure comes to the Southbound (**Device ID** and **API Key**).

The UltraLight payload can be carried out through two South Bound transport protocols:

* **HTTP** : The agent itself exposes HTTP southbound APIs, in order to accept values coming from Devices.

    *  It is available at the **public endpoint**: [https://ul-http.synchcity.eu](https://ul-http.synchcity.eu)

* **MQTT**: The agent subscribes to a device-specific topic, by using the MQTT broker Mosquitto.

    * An instance of Mosquitto is provided with the docker-compose file, it listens to the 1883 port, which is internal to the host machine and reachable by the iot-agents.

For both protocols, the IoT Agent container has a console client, for simulating the device measrements. 

**Public endpoint**: [https://ul-admin.synchcity.eu/](https://ul-admin.synchcity.eu/)

**Reference**:[ https://github.com/telefonicaid/iotagent-ul](https://github.com/telefonicaid/iotagent-ul)


## Security Layer

The security layer is in charge of providing Authentication and Authorization to the whole architecture prototype. In particular, the main focus is to grant access to the backend APIs, exposed by the described components, only to registered and authenticated users, by implementing the OAuth2 flow. 

**Authentication**

In detail, the Authentication part is composed by the IdM and Pep Proxy components.

### IdM - Keyrock

IdM offers tools for administrators to support the handling of user life-cycle functions. It reduces the effort for account creation and management, as it supports the enforcement of policies and procedures for user registration, user profile management and the modification of user accounts. It allows to link an application with the user account, in order to enable it to authenticate the users, by the interaction with the IdM Oauth APIs (see [here](http://fiware-idm.readthedocs.io/en/latest/oauth2.html)). In detail, the user will register the PEP-proxy to an application linked with its own account (see [here](http://fiware-idm.readthedocs.io/en/latest/user_guide.html) and also [here ](https://edu.fiware.org/course/view.php?id=79)for video tutorials).

The IdM is composed of two separated services, that interact with each other. The web portal is based on OpenStack’s Dashboard, **Horizon**. The back-end is a REST service based on OpenStack’s Identity Provider, **Keystone**.

Horizon internal endpoint: [http://localhost:8000](http://localhost:8000)

Keystone internal endpoint: http://localhost:8001

**Portal public endpoint:** [https://services.synchronicity-iot.eu/api/security/](https://services.synchronicity-iot.eu/api/security/)

**Reference**: [http://fiware-idm.readthedocs.io/en/latest/ ](http://fiware-idm.readthedocs.io/en/latest/)

### Pep Proxy - Wilma

The PEP Proxy GE is a backend component, without frontend interface. It is in charge of intercepting all the requests to the backend APIs and to check if the token included in the requests correspond to an authenticated user, by performing a token validation against the IdM. If the token validation succeeds, the proxy forwards finally the request to the requested API.

In order to use the secured APIs, the user must register an account in the IdM ([https://auth.synchcity.eu](https://auth.synchcity.eu)) and register a new application. It will be an application that requires to access some secured API of the platform. Then the user must register the Pep-Proxy in the application (as described in the video [here](https://edu.fiware.org/mod/url/view.php?id=735)).

All the requests must have the **X-Auth-Token** header, with the token provided by the IdM Oauth2 services. When a user logs in, IdM will generate an OAuth2 token that represents it. 

This is the "basic use case" of the Pep-Proxy User Guide ([here](http://fiware-pep-proxy.readthedocs.io/en/latest/user_guide/)), in particular the Level 1 Authentication, as depicted below. The “Backend apps” are the secured APIs exposed by the architecture components.

![image alt text](images/image_1.png)

**Reference**: [http://fiware-pep-proxy.readthedocs.io/en/latest/](http://fiware-pep-proxy.readthedocs.io/en/latest/)

### PEP Proxy - Wilma Plus
This component is an extended version of the PEP Proxy - Wilma FIWARE GE, described in the previous version. This version enables to:
- use different versions of the Keyrock IdM.
- use one PEP Proxy instance to secure both backend application (e.g. Orion) and the Historical API endpoin
- manage specific permissions and policies to your resources allowing different access levels to your users.

**Reference**
For further information about its installation and configuration, see the relative documentation in the Project repository [(here)](https://gitlab.com/synchronicity-iot/fiware-pep-proxy).

***
<a name="def-configuration"></a>
# 2. Installation and configuration

## Running with Docker compose

The whole architecture prototype will run as a set of containers, created and linked through the use of the docker-compose functionality, as defined in the provided [`docker-compose.yml`](docker-compose.yml) file. For deploying the SynchroniCity platform execute the following steps.

1. **Clone the platform repository with:**

```
git clone https://gitlab.com/synchronicity-iot/platform-deployment-docker.git
```
2. **Go inside the created folder (where it is the `docker-compose.yml`):**

```
cd platform-deployment-docker
```
3. **Perform all the modification to the provided configuration files, and if needed to the docker-compose.yml, as described in the [Configuration](#def-configuration) section below.**

4. **ONLY AFTER COMPLETING STEP 3** - Run the docker-compose file with:
```
docker-compose up
```
The containers will be automatically started and attached to the network specified in the docker-compose (*main_synchronicity*).
***

<a name="def-configuration"></a>
## Configuration


**Pay attention**
**All the modifications to the configuration files require restarting the relative container.
****DO NOT USE**** **`docker-compose down`** command, it will DESTROY all the containers of the architecture. Use instead either **`docker-compose restart`** or **`docker container_name restart`**

**Reference:** [https://docs.docker.com/compose](https://docs.docker.com/compose/)
The platform configuration, consists of two steps:
 1. Configuring components to communicate with its related Mongo persistence container.
 2. Modifying component specific configurations.

### 1. Components and related Mongo containers

As depicted in the architecture figure, each component has its related persistence container (iot-mongo -> orion , mongo-iota-manager -> iota-manager, etc).
***
#### **a. Configuring Mongo containers**

Host and port where each Mongo Container will be reachable is specified, in the relative part of the contianer, by:
- **Host**: the name of the container itself (iot-mongo, iota-manager, etc.)
- **Port**: 
    - the first part of its `ports` section (e.g. **`27017`**`:27017`)
    - the **"--port"** command line parameter in `command` section (e.g. `--port 27017`).
* In case of authenticated Mongo, set following variables in `environment` section
    * `MONGO_INITDB_ROOT_USERNAME` and `MONGO_INITDB_ROOT_PASSWORD` (e.g. root, root)
     and set, accordingly to these chosen values, the relative command line parameters in the `command` part of the containerthat uses it.

**Example**:
```
    iot-mongo:
       image: mongo:3.4
       environment:
         - MONGO_INITDB_ROOT_USERNAME=root
         - MONGO_INITDB_ROOT_PASSWORD=root
       command: mongod --nojournal --bind_ip 0.0.0.0 --port 27017
       ports:
        - "27017:27017"
       volumes:
        - './mongo_data:/data/db' 
```

#### Important note

If the authentication is enabled, the specified user, **MUST** be a superuser in MongoDB, in order to be able to perform all the CRUD operations in **ANY** DB, which are created one per tenant. In detail, it should have the following roles:
* readWriteAnyDatabase 
* dbAdminAnyDatabase 
* clusterAdmin

***
#### **b. Configuring components to use Mongo container**
The container of components that use a Mongo container, must be configured to match the configuration specified previously, namely the host and port where to reach Mongo.
In detail, depending on the particular component, they must be specified:
- as environment variables in the `environment` section
- as command line parameters in the `command` section. 

#####  Configuring mongo connnection for Orion Context Broker
The "**orion**" container uses "**iot-mongo**" persistence container. 
Add to its `command` section the following parameter:
 - `command: -dbhost iot-mongo`

If that Mongo container has been setup with authentication, add two additional command line parameters in `command` section:
 - `command: -dbhost iot-mongo -dbuser root -dbpwd root`
Change 'root' values with the ones specified when configuring the Mongo container authentication, as described in the previous section.

##### Configuring mongo connection for other components
All the oher components will manage configuration for mongo connection through environment variables, specified in their own `environment` section.
Following sections will specify the environment variables that must be set, for each specific component. 
***

### 2. Component specific configurations
The configuration files that will be modified, are provided in the root folder of this [repo](https://gitlab.com/synchronicity-iot/platform-deployment-docker) and they will be mounted as volume when the docker compose will start.


### Cygnus Connector and sinks
The Cygnus container will mount a volume, in order to install in the container the provided **`agent.conf`** configuration file. 
#### Configuring sinks
This file is already configured to setup only the **mongo-sink** and **sth-sink**, which are used by the architecture in the current version.
These sinks have the `mongo_hosts` parameter pointing to the container hostname where is deployed the mongo instance used by the **cygnus** container, namely `mongo-cygnus:27018`**

In order to correctly configure the mongo and sth sinks for Cygnus, modify, if needed the following properties.

- **`cygnus-ngsi.sinks.mongo-sink.mongo_hosts = host_and_port_where_is_mongo`**

If Mongo container authentication is enabled:
- **`cygnus-ngsi.sinks.mongo-sink.mongo_username = username (if Mongo authentication is enabled)`**

- **`cygnus-ngsi.sinks.mongo-sink.mongo_password = password *(if Mongo authentication is enabled)`**

Currently, the authentication is disabled by default.

**ImportantNote.**
When the container is restarted, its entrypoint script overwrites previous parameters in the **agent.conf** file, with the ones set in the following environment variables:

* `CYGNUS_MONGO_USER=root` **(if Mongo authentication is enabled, change with actual values)**

* `CYGNUS_MONGO_PASS=root` **(if Mongo authentication is enabled, change with actual values)**

* `CYGNUS_MONGO_HOSTS=mongo-cygnus:27018`

Thus, in order to avoid this replacement for each restart, instead of editing the agent.conf, these variables should be modified in the `environment` section of Cygnus container.

* * *
### STH-Comet 
The "**sth-comet**" container uses **mongo-cygnus**, then modify the following environment variables: 
 - `DB_URI=mongo-cygnus:27018`

### IoT Agent Manager
#### Mongo configuration
The "**iota-manager**" container uses **mongo-iota-manager**, then modify the following environment variables: 
*   `IOTA_MONGO_HOST= mongo-iota-manager`
*   `IOTA_MONGO_PORT= 27019`
  
In **mongo-iota-manager** the `ports` section has been setup with:  
	 * "**27019**:27017"
The Mongo container will be available (externally to the container itseltf), at port **27019**. For this reason we set accordingly the environment variables above.

#### Subscription configuration
The hostname of agents are used when specifying the agent URL during its subscription to the agent manager.
For instance, the "**iotagent**" field would be **http://iota-ul:4041** for subscription of the IoT Agent Ultralight.

**For further details**
[ https://github.com/telefonicaid/iotagent-manager#-subscription-api](https://github.com/telefonicaid/iotagent-manager#-subscription-api)).
* * *
<a name="def-sec-configuration"></a>
### 3. Security Configuration

As previously described, the PEP-Proxy will intercepts all the requests coming to the entrypoint of the architecture. This endpoint is represented by a Nginx Reverse Proxy that, without any security components, would redirect all the requests to the different internal endpoints corresponding to the different containers. For instance, a request to "[https://services.synchronicity-iot.eu/api/context](https://services.synchronicity-iot.eu/api/context)" coming to the 80 or 443 port of the machine hosting Docker (and then to Nginx), would be redirected (internally) to localhost:1026.

![image alt text](images/image_2.png)

The figure describes the solution provided, where was introduced the Pep-Wilma:

1. A registered user or an application registered in the user account of the IdM, sends a request with an authentication token (provided by Oauth APIs of the IdM), to the entrypoints of the architecture, as reported in  [Summary table of endpoints](#heading=h.17dp8vu)).

2. The request for https://services.synchronicity-iot.eu is redirected to the IdM, listening to the internal port 8000.

1. The requests to be protected, namely all the services.synchronicity-iot.eu/api/* (except security), are redirected to PEP-Wilma, which listens to port 81 or 444.

1. Pep Proxy validates the token against the IdM, if valid, forwards the original request to the port :8080 of proxy; otherwise, responds back with an authentication error.

2. Nginx redirects the granted request coming from Pep Proxy, to the appropriate container port, according to the host name present in the original request.

For further details, see the provided Nginx configuration file (default) and the documentation for Nginx as reverse proxy ([here](https://www.nginx.com/resources/admin-guide/reverse-proxy/)). For configuring NGINX in HTTPS, please consider to use CertBot (documentation [here](https://certbot.eff.org/all-instructions/#ubuntu-16-04-xenial-nginx)), which automatically generates and installs certificates both in the machine running Nginx and in its configuration file (located in **/etc/nginx/site-available/default**).

 *** 
**Pay attention**
**All the modifications to the configuration files require restarting the relative container.
****DO NOT USE**** **`docker-compose down`** command, it will DESTROY all the containers of the architecture. Use instead either **`docker-compose restart`** or **`docker container_name restart`**
### Idm Configuration
***
The IdM container will mount a volume, in order to install in the container the provided `local_settings.py` configuration file. This file must be modified, either before launching “docker-compose up” and then mounting it in the container or after the container startup, by entering in the container itself and modifying the file in the `“/horizon/openstack_dashboard/local/local_settings.py'` path. 

The following configurations should be modified (see [here](http://fiware-idm.readthedocs.io/en/latest/setup.html) for other recommended production setups).

**Email**

By default Keyrock prints the registration confirmation email in console, this behavior **MUST** be avoided, in order to receive a real mail and to active a newly created account. It is needed to install a SMTP server, either in the IDM container or in the host machine: the typical choice is POSTFIX (see [here ](https://help.ubuntu.com/lts/serverguide/postfix.html)for installation).

In the `local_settings.py` modify the following variables:

- **`EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'`**
- Change **`EMAIL_HOST`** and **`EMAIL_PORT`** parameters according to the host where is reachable the installed SMTP server (e.g. POSTFIX). 
	* **`EMAIL_HOST = '172.18.0.1'`**
	* **`EMAIL_PORT = 25`**

**Note** .In our dockerized environment, we could install the SMTP server in the host machine, thus we have to use as EMAIL_HOST the IP of the docker network gateway.

- Change the `EMAIL_URL` parameter with the one where the IdM is reacheable (in the prototype case in the Nginx entrypoint).

	*	**`EMAIL_URL = 'https://synchronicity-iot.eu'`**

- Change the following self-explaining parameters:
	* **`DEFAULT_FROM_EMAIL = 'your-no-reply-address'`**
	* **`EMAIL_SUBJECT_PREFIX = '[Prefix for emails subject]'`**

1. If the SMTP server requires authentication (not needed in the prototype, as POSTFIX is reachable on port 25 only internally).
	* **`EMAIL_HOST_USER = 'username'`**
	* **`EMAIL_HOST_PASSWORD ='password'`**

### **Pep-Proxy Wilma Configuration**
We are two options for using the PEP Proxy Wilma components:
1) If you want to use the original Fiware version of the PeP proxy, read the following section. 
2) If instead, you want to use the "Plus" version, please see the installation documentation [(here)](https://gitlab.com/synchronicity-iot/fiware-pep-proxy)

The Pep-Proxy will mount a volume, in order to install in the container the provided **`config.js`** configuration file. This file should be modified, either before launching “docker-compose up” and then mounting it in the container or after the container startup, by entering in the container itself and modifying the file in the **“/opt/fiware-pep-proxy/config.js'** path. 

#### Fiware Pep-Proxy Configuration
Edit the `config.js` file mounted as volume in the PEP Proxy container, by modifying the following relevant part:
```
// Used only if https is disabled
config.pep_port = 81;

// Set this var to undefined if you don't want the server to listen on HTTPS
config.https = {
    enabled: false,
    cert_file: '/etc/ssl/certs/pep-selfsigned.crt',
    key_file: '/etc/ssl/certs/pep-selfsigned.key',
    port: 444
};

config.account_host = 'http://localhost:8000';

config.keystone_host = 'localhost';
config.keystone_port = 8001;

config.app_host = 'localhost';
config.app_port = '8080';
// Use true if the app server listens in https
config.app_ssl = true;

// Credentials obtained when registering PEP Proxy in Account Portal
config.username = 'pep_proxy_9c5c15315a1f48bb8a0de6dbf46f7a8d';
config.password = 'c3049fc7903544e3a81077530391b2b3';
```

* **`config.pep_port`**: where the PeP-Proxy will listen to (in HTTP)

* **`config.https`**: if enabled the Pep-Proxy will listen only to the specified port. Note that the cert_file and keyfile are to be correctly configured, targeting either a self-signed certificate and file or the one generated by Certbot.

* **`config.account_host`**: the host where is reacheable the IdM (Horizon)

* **`config.app_host`** and config.app_port: host and port of the secured endpoint (in this case it is where is listening the Nginx endpoint that redirects to all the actual endpoints of the components.  

* **`config.app_ssl`** = true;  Use true if the app server listens in https

* **`config.username and config.password`**: they are the username and password provided by the IdM portal when registering the Pep-Proxy to the application.

**IMPORTANT NOTE**. In order to apply the configuration modified above, the PEP Proxy container **MUST BE** restarted, by running `docker CONTAINER_NAME restart` command

<a name="def-examples"></a>
# 4. Usage Scenarios examples and annotations
 
## Orion context Broker Multiservices

The Orion Context Broker implements a simple multitenant / multiservice model based and logical database separation. Multitenant / multiservice ensures that the entities/attributes/subscriptions of one service/tenant are "invisible" to other services/tentants. 

This functionality is activated in the SynchroniCity prototype, Orion uses the "Fiware-Service" and (optional for subpath) "Fiware-ServicePath" HTTP headers in the request to identify the service/tenant. If the headers are not present in the HTTP request, the default service/tenant is used.

### **Example**

For example, queryContext on tenantRZ1 space will never return entities/attributes from tenantRZ2 space.

* Create a device with Fiware-Service "**tenantRZ1**" and id “**device1**”.

![image alt text](images/image_3.png) 

* Get the newly created device in this service/tenant, note the value "**10.0"** for the **temperature** attribute.

![image alt text](images/image_4.png)

* Create a device with Fiware-Service "**tenantRZ2**" and id “**device1**”.

![image alt text](images/image_5.png)

![image alt text](images/image_6.png)

* Get the newly created device in this service/tenant, note the value "**20.0"** for the **temperature** attribute. Despite the id is the same of the previous one (**device1**), the temperature has another value, because we are in a different tenant (**tenantRZ2**) and indeed, this is another entity.

* * *


## Scenario 1 (COAP w/o IoTA Manager)

![image alt text](images/image_7.png)

## Scenario 2 (with IoTA Manager)

The following Scenario describes the interaction of the IoT-Agent Manager that acts as proxy for the provisioning of the IoT Agents, related devices and services/tenants.

As described in [https://github.com/telefonicaid/iotagent-manager#-subscription-api](https://github.com/telefonicaid/iotagent-manager#-subscription-api) :

1. Register IoT Agent (iota-ul) to IoTA Manager (iota-manager) with service (tenant):

	* Fiware-Service and ServicePath: **myhome**, **/environment**

	* Protocol: **IoTA-UltraLight**

	* Resource: **/iot/d**

	* IotAgent: [http://iota-ul:4041](http://iota-ul:4041) (**iota-ul**, as like the following ones contained in any URL fields, is the hostname visible to the IotA Manager, due to the Docker linkage process, as described in the "Docker compose configuration" section).

![image alt text](images/image_8.png)

* * *


2. Create a new service associated with a tenant (**myhome/environment)** and a protocol (the agent protocol previously created, e.g. **IoTA-UltraLight**). In this case, as shown in the following body request, the "**service**" to be created is considered as a common configuration for all the devices belonging to this tenant. This request is forwarded to the agent specified in the “**protocol**" query parameter. Indeed, the agent provides the same API, but in this case the “service” is created both in the IoTA-Manager and then in the registered IoT Agent.

![image alt text](images/image_9.png)

In this example, after the "service" creation, all the devices having the same **apikey-resource** pair value, will have the same base configuration. For example, all these devices will have the **status** active attribute. 

***

3. Register a device to the registered IoT Agent through IoTA Manager by specifying the agent protocol in the query parameter (**IoTA-UltraLight)**, the tenant headers (Fiware-Service and ServicePath: **myhome**, **/environment)**, in addition to the following fields relative to the device and the resulting mapped entity:

	* Device_id: **sensor01**

	* Entity name and Type: **LivingRoomSensor**, **multiSensor**

	* Attributes:  object Id --> name, type (mapping between device and entity attributes)

![image alt text](images/image_10.png)

As shown below, the device was registered to its Agent. The attributes array contains the mapping between a device attribute (**object_id**) and the resulting entity attribute (**name-type** pair).

![image alt text](images/image_11.png)

* * *


4. The IoT Agent creates the mapped entity, by issuing a NGSI request to the Context Broker, corresponding to its registered device. Even in this case, this API could be invoked directly against the Agent, but in this way the IoTA Manager is used as a proxy and single provisioning point.

	As shown below, by querying the Context Broker for the entities belonging to the (**myhome/environment) **tenant, we get the mapped entity **"LivingRoomSensor"**.

![image alt text](images/image_12.png)

* * *


5. The registered device sends a measure via HTTP (in case of active attribute) 		to the IoT UltraLight Agent, which translates the payload to a NGSI request and sends it to the ContextBroker. As shown below, we simulate a device sending measurements, through the HTTP southbound endpoint exposed by IoTAgent.

![image alt text](images/image_13.png)

In this case, we send the value **29.5 **for the attribute **t** of the device with id **sensor01 **and apikey **ABCDEF**.

* * *


6. The attribute value (**temperature**) of the mapped entity is updated in the Context Broker with the value (**29.5)**.

![image alt text](images/image_14.png)

* * *


7. If there is a subscription with the appropriate endpoint, Cygnus through its configured sinks (**mongo-sink** and **sth-sink**) stores historical raw and aggregated context information in the mongo container (**mongo-cygnus**). 
* Create the subscription in the Orion Context Broker for the mapped entity. Note the **reference** field, it points to the Cygnus endpoint .

![image alt text](images/image_15.png)

- Check in the **mongo-cygnus** instance with a Mongo Client (e.g. **Robo 3T**) that the following have been created:

* **mongo_myhome** DB for historical raw data with the 	**mongo_/environment_LivingRoomSensor_multiSensor** collection. It contains the entry with the attribute (**temperature - 29.5**) that was just updated.

![image alt text](images/image_16.png)

* **sth_myhome** DB for historical aggregated data with the **sth_/environment_LivingRoomSensor_multiSensor.aggr** collection. It contains a set of documents representing points calculated for the aggregated historical views.

![image alt text](images/image_17.png)

* * *


8.  Retrieve historical raw context information through REST API exposed by the STH component. In particular we get the last **3** values for the **Temperature** attribute of **LivingRoomSensor** entity.

![image alt text](images/image_18.png)

* * *


9. Retrieve historical aggregated context information though REST API exposed by the STH component. In particular we get the **minimum** value of the **Temperature** attribute, with a time resolution of a **day** (the minimum temperature value in a day).

![image alt text](images/image_19.png)

<a name="def-endpoints"></a>
# 5. **Summary table of public instance endpoints**
This table reports the public endpoints where the reference instance of the SynchroniCity project is available.
|Public Endpoint|Description|
|----------------------------------------------------------------------------------|----------------------------------------------------------------------|
|https://services.synchronicity-iot.eu/api/context|Orion Context Broker endpoint|
|https://cygnus.synchcity.eu|Cygnus administration endpoint|
|https://iot-agent.synchcity.eu|IoT Agent Manager|
|https://ul-http.synchcity.eu|http endpoint accepting Ultra Light payload from known devices|
|https://ul-admin.synchcity.eu|UltraLight Agent administration endpoint|
|https://services.synchronicity-iot.eu/api/historical|STH-Comet-SynchroniCity Historical API endpoint|
|https://services.synchronicity-iot.eu|IdM Portal Endpoint|

