var config = {};

// Used only if https is disabled
config.pep_port = 7000;

// Set this var to undefined if you don't want the server to listen on HTTPS
config.https = {
    enabled: false,
    cert_file: 'cert/cert.crt',
    key_file: 'cert/key.key',
    port: 443
};

//== LINK to IdM (Need to provide the access (port) of Keystone component for versions under KR7) ===
config.idm = {
    version: 'keyrock:7',
	host: 'keyrock.docker',
	port: '3000',
	ssl: false
}

//== LINK to HISTORICAL API (TEST_ENV) [REQUESTS FORWARDING] ========================================
config.app_host = 'historical-api.docker';
config.app_port = '8080';
// Use true if the app server listens in https
config.app_ssl = false;


//== Credentials (For REGISTERED APP - Orion/Historical -) provided by KeyRock ======================
config.pep = {
	app_id: 'a4c552ce-179d-4a28-afe9-5a51dddb7c8c',
	username: 'pep_proxy_cd3d36b7-0834-4579-b08b-91ce2bf64632',
	password: 'pep_proxy_cb1237df-6e74-4f41-ab17-2560be430d4a',
	trusted_apps : []
}

// in seconds
config.cache_time = 300;

//== Configuring RBAC ===============================================================================
// if enabled PEP checks permissions of NGSIv2 request with a Role Based Access Control
// roles have to be provided according to the following scheme:
// fiware-service|operation|entityType|entityID|attribute
// e.g., tenantRZ1|GET|AirQualityObserved||
// the above example will grant GET permission for each entity of type AirQualityObserved under the Fiware-Service tenantRZ1 
config.rbac = false;

//== Logs in MongoDB ================================================================================
//if enabled PEP logs access requests and responses on mongoDb
config.logging = true;
//== MongoDB
config.mongoDb = {
    server: 'mongo-pep.docker',
    port: 27017,
    user: '',
    password: '',
    db: 'pep-historical'
};

//== if enabled PEP checks permissions with AuthZForce GE. ==========================================
// only compatible with oauth2 tokens engine
//
// you can use custom policy checks by including programatic scripts 
// in policies folder. An script template is included there
config.azf = {
    enabled: false,
    protocol: 'http',
    host: 'pdp.docker',
    port: 8080,
    custom_policy: undefined // use undefined to default policy checks (HTTP verb + path).
};

// list of paths that will not check authentication/authorization
// example: ['/public/*', '/static/css/']
config.public_paths = [];

config.magic_key = '123456789';

module.exports = config;
